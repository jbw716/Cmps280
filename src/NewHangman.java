// CMPS 280
// Hangman
// Phillip Weaver
// W0659336
// 4/24/2017
// This Program is brought to you by: Coffee!

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class NewHangman {

    public static void main(String[] args) {

        System.out.println("Hangman Game");

        do {

            String word = getWord();

            String stars = stars(word);

            //System.out.println(word);

            //System.out.println(stars);

            guess(word, stars);

        }while(again() == 'y');

    }

    /**Method for getting a random word from the wordlist.txt file**/

    public static String getWord() {

        File wordList = new File("wordlist.txt");

        if(!wordList.exists())
            wordList = new File("src/wordlist.txt");

        ArrayList<String> words = new ArrayList<>();

        try( Scanner wlscanner = new Scanner(wordList) ){

            while(wlscanner.hasNext())
                words.add(wlscanner.next());

        }

        catch (FileNotFoundException e){

            System.out.println("System could not find the wordlist.txt file!");

            System.exit(1);

        }

        int index = (int)((Math.random() * words.size()));

        return words.get(index);

    }

    /**Processing for stars to represent letters of the hidden word**/

    public static String stars(String word) {

        String stars = "";

        for(int i = 0; i < word.length(); i++){

            stars += "*";

        }

        return stars;

    }

    /**Processing for player's guessing and scorekeeping**/

    public static void guess(String word, String starsOld){

        StringBuilder stars = new StringBuilder(starsOld);

        Scanner input = new Scanner(System.in);

        int missed = 0, correct = 0;

        ArrayList<Character> guessedChars = new ArrayList<>();

        do{

            System.out.println("\nYour current score is " + correct + " correct and " + missed + " missed.");

            System.out.println(stars);

            System.out.print("\nEnter your guess: ");

            char guess = input.nextLine().toLowerCase().charAt(0);

            if (stars.indexOf(guess + "") >= 0) {

                System.out.println("\t'" + guess + "' is already in the word.");

            } else if (word.indexOf(guess) < 0) {

                if(guessedChars.contains(guess))
                    System.out.println("\tYou already guessed '" + guess + "'.");

                else {

                    System.out.println("\t'" + guess + "' is not in the word");

                    guessedChars.add(guess);

                    missed++;

                }

            } else {

                System.out.println("\t'" + guess + "' is in the word!");

                int k = word.indexOf(guess);

                while (k >= 0) {

                    stars.setCharAt(k, guess);

                    correct++;

                    k = word.indexOf(guess, k + 1);

                }

            }

        }while(stars.toString().contains("*"));

        System.out.println("\nCongratulations! The word was '" + word + "'.\nYou successfully guessed the word in " + (correct + missed) + " guesses, having " + correct + " correct, and " + missed + " missed.");

    }

    /**Check to see if player wants to play again**/

    public static char again(){

        while(true){

            System.out.print("\nWould you like to play again (y/n)?\n>");

            Scanner input = new Scanner(System.in);

            char response = input.nextLine().toLowerCase().charAt(0);

            if(!(response == 'y' || response == 'n')){

                System.out.println("Sorry. That is not a valid answer. Please enter 'y' ir 'n'.");

            }

            else
                return response;

        }

    }

}
