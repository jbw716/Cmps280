// CMPS 280
// Program Assignment 01
// Phillip Weaver
// W0659336
// 1/23/2017
// This Program is brought to you by: Coffee!

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Grader {

    public static void main(String args[]) throws InterruptedException {

        System.out.println("******************\n* Grader Program *\n******************");

        logic();

    }

    public static void logic() throws InterruptedException {

        Scanner input = new Scanner(System.in);

        try {

            System.out.print("\nPlease enter your number grade: ");

            double Grade = input.nextDouble();

            System.out.print("\nYou entered: " + Grade + "%\n");

            TimeUnit.MILLISECONDS.sleep(500);

            if (Grade > 100) error();

            else if (Grade >= 90) System.out.print("\nYou received an A! Good job!\n");

            else if (Grade >= 80) System.out.print("\nYou received a B! Good job!\n");

            else if (Grade >= 70) System.out.print("\nYou received a C.\n");

            else if (Grade >= 60) System.out.print("\nYou received a D.\n");

            else if (Grade >= 0) System.out.print("\nYou received an F. Uh-oh...\n");

            else if (Grade < 0) error();

        }

        catch(InputMismatchException e){

            error();

        }

    }

    public static void error() throws InterruptedException {

        System.out.print("\nAn error occurred!\n");

        TimeUnit.MILLISECONDS.sleep(500);

        logic();

    }

}
