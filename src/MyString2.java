// CMPS 280
// MyString2
// Phillip Weaver
// W0659336
// 3/29/2017
// This Program is brought to you by: Coffee!

public class MyString2 {

    private String usedString;

    private char[] chars;

    public static void main(String[] args){

        MyString2 string1 = new MyString2("Howdy234");

        MyString2 upperS1 = string1.toUpperCase();

        MyString2 upperS1Sub = upperS1.substring(3);

        MyString2 upperS1SubNeg = upperS1.substring(-4);

        upperS1.printMyString2();

        upperS1Sub.printMyString2();

        upperS1SubNeg.printMyString2();

        MyString2 bool = MyString2.valueOf(true);

        bool.printMyString2();

        System.out.println(string1.compare(bool.getUsedString()));

        char[] upperS1Arr = upperS1.toChars();

        for(int i = 0; i < upperS1Arr.length; i++){

            System.out.println(upperS1Arr[i]);

        }

    }

    public MyString2(String s){

        this.usedString = "";

        for(int i = 0; i < s.length(); i++){

            this.usedString += s.charAt(i) + "";

        }

        this.chars = new char[s.length()];

        for(int i = 0; i < chars.length; i++){

            chars[i] = s.charAt(i);

        }

    }

    public void printMyString2(){

        System.out.println(usedString);

    }

    public String getUsedString(){

        return usedString;

    }

    public int compare(String s){

        if(s.length() != usedString.length())

            return 0;

        for(int i = 0; i < usedString.length(); i++){

            if(usedString.charAt(i) != s.charAt(i))

                return 0;

        }

        return 1;

    }

    public MyString2 substring(int begin){

        String sub = "";

        if(begin < 0){

            String revearsed = "";

            String subRev = "";

            for(int i = usedString.length(); i > 0; i--){

                revearsed += usedString.charAt(i-1);

            }

            for(int i = (begin * -1); i < revearsed.length(); i++) {

                subRev += revearsed.charAt(i);

            }

            for(int i = subRev.length(); i > 0; i--){

                sub += subRev.charAt(i-1);

            }

        }

        else {

            for (int i = begin; i < usedString.length(); i++) {

                sub += usedString.charAt(i) + "";

            }

        }

        return new MyString2(sub);

    }

    public MyString2 toUpperCase(){

        String upper = "";

        for(int i = 0; i < usedString.length(); i++){

            if(usedString.charAt(i) >= 'a' && usedString.charAt(i) <= 'z') {

                upper += ((char)(usedString.charAt(i) - 32));

            }

            else{

                upper += (usedString.charAt(i));

            }

        }

        return new MyString2(upper);

    }

    public char[] toChars(){

        char[] returnArr = new char[usedString.length()];

        for(int i = 0; i < usedString.length(); i++){

            returnArr[i] = usedString.charAt(i);

        }

        return returnArr;

    }

    public static MyString2 valueOf(boolean bool){

        return (bool ? new MyString2("true") : new MyString2("false"));

    }

}
