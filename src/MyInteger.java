// CMPS 280
// My Integer
// Phillip Weaver
// W0659336
// 3/24/2017
// This Program is brought to you by: Coffee!

/*
 UML Diagram
 ----------------------------------------
 - value: int
 + MyInteger(value: int)
 ----------------------------------------
 + getValue(): int
 + isEven(): boolean
 + isOdd(): boolean
 + isPrime(): boolean
 + isEven(value: int): boolean
 + isOdd(value: int): boolean
 + isPrime(value: int): boolean
 + isEven(value: MyInteger): boolean
 + isOdd(value: MyInteger): boolean
 + isPrime(value: MyInteger): boolean
 + equals(value: int): boolean
 + equals(value: MyInteger): boolean
 + parseInt(chars: char[])
 + parseInt(str: String)
*/

public class MyInteger {

    private int value;

    public MyInteger(int value){

        this.value = value;

    }

    public int getValue(){

        return value;

    }

    public boolean isEven() {

        return isEven(value);

    }

    public boolean isOdd() {

        return isOdd(value);

    }

    public boolean isPrime() {

        return isPrime(value);

    }

    public static boolean isEven(int value) {

        return value % 2 == 0;

    }

    public static boolean isOdd(int value) {

        return value % 2 != 0;

    }

    public static boolean isPrime(int value) {

        for (int i = 2; i <= value / 2; i++) {

            if (value % i == 0)

                return false;

        }

        return true;

    }

    public static boolean isEven(MyInteger myInt) {

        return myInt.isEven();

    }

    public static boolean isOdd(MyInteger myInt) {

        return myInt.isOdd();

    }

    public static boolean isPrime(MyInteger myInt) {

        return myInt.isPrime();

    }

    public boolean equals(int value) {

        return this.value == value;

    }

    public boolean equals(MyInteger myInt) {

        return myInt.value == this.value;

    }

    public static int parseInt(char[] passArray){

        int returnInt;

        String intString = "";

        for(int i = 0; i < passArray.length; i++){

            intString += passArray[i];

        }

        returnInt = Integer.parseInt(intString);

        return returnInt;

    }

    public static int parseInt(String passString){

        return Integer.parseInt(passString);

    }

}
