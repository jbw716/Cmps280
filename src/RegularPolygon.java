// CMPS 280
// Regular Polygon Assignment
// Phillip Weaver
// W0659336
// 3/3/2017
// This Program is brought to you by: Coffee!

public class RegularPolygon {

    // Default 3-sided polygon (Triangle).

    private int n = 3;

    //Side length. Default 1

    private double side = 1;

    //x-coordinate of center of polygon

    private double x = 0;

    //y-coordinate of center of polygon

    private double y = 0;

    //Count of polygons

    private static int count;

    RegularPolygon(){

        count++;

        printPerimeter();

        printArea();

    }

    RegularPolygon(int newN, double newSide){

        n = newN;

        side = newSide;

        count++;

        printPerimeter();

        printArea();

    }

    RegularPolygon(int newN, double newSide, double newX, double newY){

        n = newN;

        side = newSide;

        x = newX;

        y = newY;

        count++;

        printPerimeter();

        printArea();

    }

    void printPerimeter(){

        System.out.println("\nThe perimeter of the polygon with " + n + " sides of length " + side + " at coordinates (" + x + "," + y + ") is: " + getPerimeter());

    }

    double getPerimeter(){

        return side * n;

    }

    void printArea(){

        System.out.println("\nThe area of the polygon with " + n + " sides of length " + side + " at coordinates (" + x + "," + y + ") is: " + Math.round(getArea()*1000)/1000.0);

    }

    double getArea(){

        return (n * (side * side))/(4 * Math.tan(Math.PI/n));

    }

    int getN(){

        return n;

    }

    double getSide(){

        return side;

    }

    double getX(){

        return x;

    }

    double getY(){

        return y;

    }

    int getCount(){

        return count;

    }

}
