// CMPS 280
// Program Assignment 01
// Phillip Weaver
// W0659336
// //
// This Program is brought to you by: Coffee!

public class Rectangle {

    double length;

    double width;

    static int count;

    public static void main(String[] args){

        printCount();

        Rectangle rectangle = new Rectangle();

        printCount();

        Rectangle zackRectangle = new Rectangle(10,5);

        printCount();

        System.out.println("Area is " + rectangle.getArea());

        rectangle.setWidth(4);

        rectangle.setLength(6);

        System.out.println("Area is " + rectangle.getArea());

        System.out.println("Area is " + zackRectangle.getArea());

    }

    Rectangle(double newWidth, double newLength){

        width = newWidth;

        length = newLength;

        count++;

    }

    Rectangle(){

        count++;

    }

    public static int getCount(){

        return count;

    }

    public static void printCount(){

        System.out.println("Count is " + getCount());

    }

    public double getArea(){

        double area = length * width;

        return area;

    }

    public void setWidth(double newWidth){

        width = newWidth;

    }

    public void setLength(double newLength){

        length = newLength;

    }

}
