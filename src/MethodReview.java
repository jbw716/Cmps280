// CMPS 280
// Program Assignment 01
// Phillip Weaver
// W0659336
// //
// This Program is brought to you by: Coffee!

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class MethodReview {

    public static void main(String[] args) throws InterruptedException {

        try{

            logic();

        }

        catch(InputMismatchException e){

            System.out.print("\nSorry. That is not a valid input. Please try again...\n");

            TimeUnit.MILLISECONDS.sleep(500);

            logic();

        }

    }

    public static void logic() throws InterruptedException {

        Scanner input = new Scanner(System.in);

        System.out.print("\nPlease enter two integers: ");

        int value1 = input.nextInt();

        int value2 = input.nextInt();

        System.out.print("\nThe max value when given " + value1 + " and " + value2 + ", is " + max(value1,value2) + "\n");

        TimeUnit.MILLISECONDS.sleep(500);

        while(true) {

            System.out.print("\nWould you like to run another calculation? (y/n)\n");

            input.nextLine();

            String answer = input.nextLine();

            if (answer.toLowerCase().startsWith("y")){

                logic();

                break;

            }

            else if (answer.toLowerCase().startsWith("n")){

                break;

            }

            else System.out.print("\nSorry. That is not a valid input...\n");

        }

    }

    public static int max(int value1, int value2){

        int maxValue = 0;

        if(value1 > value2)
            maxValue = value1;

        else
            maxValue = value2;

        return maxValue;


    }

}


//Method 1: Main
//Method 2: Return letter grade corresponding to numeric grade passed

//Print out numbers beginning with int1 and ending with int2
//pass int1 and int2
//given 12 and 15, print 12, 13, 24, 15

//3. Method allows user to input numbers until he indicates he is finished. The sum of these values is returned. While Loop