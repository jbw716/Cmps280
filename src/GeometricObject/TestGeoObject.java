package GeometricObject;
// CMPS 280
// Assignment Name
// Phillip Weaver
// W0659336
// //
// This Program is brought to you by: Coffee!

public class TestGeoObject {

    public static void main(String[] args){

        Circle circle = new Circle();

        System.out.println(circle.getDateCreated());

        Circle circle1 = new Circle(10);

        System.out.println(circle1.getRadius());

        System.out.println(circle.getRadius());

        Circle circle2 = new Circle(15, "clear", true);

        Square square = new Square(2, "clear", false);

        System.out.println(square.getHeight() + " " + square.getWidth() + " " + square.getArea() + " " + square.getPerimeter() + " " + square.getDateCreated() + " " + square.getColor());

        System.out.println(square.toString());

        square.setColor("red");

        square.setFilled(true);

        square.setSideLen(4);

        System.out.println(square.toString());

        square.setWidth(10);

        square.setHeight(10);

        Rectangle rectangle = new Rectangle(2, 5, "clear", true);

        rectangle.setWidth(5);

        System.out.println(rectangle.getWidth());

    }

}
