package GeometricObject;
// CMPS 280
// Square Subclass
// Phillip Weaver
// W0659336
// 4/6/2017
// This Program is brought to you by: Coffee!

/***********************************************************
 *    Square Extends Rectangle Extends GeometricObject     *
 *---------------------------------------------------------*
 * +Square()                                               *
 * +Square(sideLen: double)                                *
 * +Square(sideLen: double, color: String, filled: boolean)*
 * +setSideLen(sideLen: double): Void                      *
 * +getSideLen(): double                                   *
 * +setWidth(): Void                                       *
 * +setHeight(): Void                                      *
 * +disclaimer(): Void                                     *
 ***********************************************************/

public class Square extends Rectangle {

    public Square(){

        disclaimer();

    }

    public Square(double sideLen){

        super(sideLen, sideLen);

        disclaimer();

    }

    public Square(double sideLen, String color, boolean filled){

        super(sideLen, sideLen, color, filled);

        disclaimer();

    }

    public void setSideLen(double sideLen){

        super.setHeight(sideLen);

        super.setWidth(sideLen);

    }

    public double getSideLen(){

        return super.getHeight();

    }

    @Override
    public void setWidth(double width){

        throw new UnsupportedOperationException("Sorry. You cannot use the 'setWidth' command with a 'Square' object. Please use 'setSideLen' instead.");

    }

    @Override
    public void setHeight(double height){

        throw new UnsupportedOperationException("Sorry. You cannot use the 'setHeight' command with a 'Square' object. Please use 'setSideLen' instead.");

    }

    public static void disclaimer(){

        System.out.println("A square has to have 4 sides of equal length. If you would like sides of different lengths, please create a 'Rectangle' instead.");

    }

}
