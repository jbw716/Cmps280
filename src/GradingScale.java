// CMPS 280
// Program Assignment 02
// Phillip Weaver
// W0659336
// 1/23/2017
// This Program is brought to you by: Coffee!

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class GradingScale {

    public static void main(String args[]) throws InterruptedException {

        //Greet the user with a program header and initiate the logic of the program.

        System.out.println("*************************\n* Grading Scale Program *\n*************************\n");

        logic();

    }

    //Main logic of the program.

    public static void logic() throws InterruptedException {

        Scanner input = new Scanner(System.in);

        while(true) {

            //Prompt the user for an input.

            System.out.print("Please enter the letter grade that you would like to check the score range for and then press 'ENTER'.\nPress 'ENTER' without entering anything to end the program.\n> ");

            String userInput = input.nextLine();

            //End the loop if the user's input is blank.

            if(userInput.equals("")){

                System.out.print("\nProgram ending...\n");

                break;

            }

            //Error check. Throw error and restart the loop of the user entered more than one character.

            else if (userInput.length() > 1){

                System.out.print("\nAn error occurred!\n");

                TimeUnit.MILLISECONDS.sleep(500);

                continue;

            }

            //If "end" and error checks passed, continue to the switch statment.

            else {

                char grade = userInput.charAt(0);

                switch (grade) {

                    case 'A':

                    case 'a':
                        System.out.print("\n90-100\n\n");
                        TimeUnit.MILLISECONDS.sleep(500);
                        break;

                    case 'B':

                    case 'b':
                        System.out.print("\n80-89\n\n");
                        TimeUnit.MILLISECONDS.sleep(500);
                        break;

                    case 'C':

                    case 'c':
                        System.out.print("\n70-79\n\n");
                        TimeUnit.MILLISECONDS.sleep(500);
                        break;

                    case 'D':

                    case 'd':
                        System.out.print("\n60-69\n\n");
                        TimeUnit.MILLISECONDS.sleep(500);
                        break;

                    case 'F':

                    case 'f':
                        System.out.print("\n0-59\n\n");
                        TimeUnit.MILLISECONDS.sleep(500);
                        break;

                    //Throw an error and restart the loop if the user input one character, but it is not a valid letter grade.

                    default:
                        System.out.print("\nAn error occurred!\n");
                        TimeUnit.MILLISECONDS.sleep(500);
                        break;

                }

            }

        }

    }

}
