// CMPS 280
// Hangman
// Phillip Weaver
// W0659336
// 4/24/2017
// This Program is brought to you by: Coffee!

/******************************
 *          Hangman           *
 *----------------------------*
 * +main                      *
 *                            *
 * +getWords(): String[]      *
 *****************************/

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Hangman {
  public static void main(String[] args) {

    /**---Changes Start---**/

    String[] words = getWords();

    /**---Changes End---**/

    char anotherGame;

    Scanner input = new Scanner(System.in);

    do {
      int index = (int) (Math.random() * words.length);

      String hiddenWord = words[index];
      StringBuilder guessedWord = new StringBuilder();

      for (int i = 0; i < hiddenWord.length(); i++)
        guessedWord.append('*');

      int numberOfCorrectLettersGuessed = 0, numberOfMisses = 0;

      while (numberOfCorrectLettersGuessed < hiddenWord.length()) {
        System.out.print("(Guess) Enter a letter in word " + guessedWord
            + " > ");
        String s = input.nextLine();

        /**---Additions Start---**/

        if(s.equals(""))
          continue;

        /**---Additions End---**/

        char letter = s.charAt(0);

        if (guessedWord.indexOf(letter + "") >= 0) {
          System.out.println("\t" + letter + " is already in the word");
        } else if (hiddenWord.indexOf(letter) < 0) {
          System.out.println("\t" + letter + " is not in the word");
          numberOfMisses++;
        } else {
          int k = hiddenWord.indexOf(letter);
          while (k >= 0) {
            guessedWord.setCharAt(k, letter);
            numberOfCorrectLettersGuessed++;
            k = hiddenWord.indexOf(letter, k + 1);
          }
        }
      }

      System.out.println("The word is " + hiddenWord + ". You missed "
          + numberOfMisses + ((numberOfMisses <= 1) ? " time" : " times"));

      System.out.print("Do you want to guess for another word? Enter y or n> ");
      anotherGame = input.nextLine().charAt(0);
    } while (anotherGame == 'y');
  }

  /**---Additions Start---**/

  public static String[] getWords(){
    ArrayList<String> words = new ArrayList<>();
    File wordList = new File("wordlist.txt");
    if(!wordList.exists())
      wordList = new File("src/wordlist.txt");
    try( Scanner wlscanner = new Scanner(wordList) ){
      while(wlscanner.hasNext())
        words.add(wlscanner.next().toLowerCase());
    }
    catch (FileNotFoundException e){
      System.out.println("System could not find the wordlist.txt file!");
      System.exit(1);
    }

    return words.toArray(new String[0]);
  }

  /**---Additions End---**/

}