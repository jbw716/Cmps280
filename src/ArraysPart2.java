// CMPS 280
// Program Assignment 03 Part 2
// Phillip Weaver
// W0659336
// 2/2/2017
// This Program is brought to you by: Coffee!

public class ArraysPart2 {

    public static void main(String[] args){

        double[] array = new double[10];

        for(int i = 0; i < array.length; i++){

            array[i] = (Math.round(Math.random() * 10000.0))/100.0;

        }

        System.out.print("Initial Array Initialized With Random Values:");

        printIntArray(array);

        System.out.print("\n\nSum of all Array Values:\n" + sum(array));

        System.out.print("\n\nLargest Value in the Array:\n" + largest(array));

        System.out.print("\n\nSmallest Index of the Largest Value in the Random Array:\n" + smIndexLgElement(array));

        double[] tempArray = {1,3,5,3,2,5};

        System.out.print("\n\nSmallest Index of the Largest Value in the Array {1,3,5,3,2,5}:\n" + smIndexLgElement(tempArray));

        System.out.print("\n\nShuffled Array:");

        shuffle(array);

        printIntArray(array);

        System.out.print("\n\nNew Smallest Index of the Largest Value in the Array:\n" + smIndexLgElement(array));

        System.out.print("\n\nArray With all Values Shifted One Index Up. The Last Value Was Moved to Index 0: ");

        shift(array);

        printIntArray(array);

        System.out.print("\n\nNew Smallest Index of the Largest Value in the Array:\n" + smIndexLgElement(array));

    }

    public static void printIntArray(double[] passArray){

        System.out.print("\n{");

        for(int i = 0; i < passArray.length; i++){

            System.out.print(passArray[i]);

            if(i != (passArray.length - 1)) System.out.print(", ");

            else System.out.print("}");

        }

    }

    public static double sum(double[] passArray){

        double returnSum = 0;

        for(int i = 0; i < passArray.length; i++){

            returnSum += passArray[i];

        }

        return (Math.round(returnSum * 1000.0))/1000.0;

    }

    public static double largest(double[] passArray){

        double returnValue = passArray[0];

        for(int i = 1; i < passArray.length; i++){

            if(returnValue < passArray[i]) returnValue = passArray[i];

        }

        return returnValue;

    }

    public static int smIndexLgElement(double[] passArray){

        int returnValue = 0;

        for(int i = 0; i < passArray.length; i++){

            if(passArray[returnValue] < passArray[i]){

                returnValue = i;

            }

        }

        return returnValue;

    }

    public static void shuffle(double[] passArray){

        int randNum;

        for(int i = 0; i < passArray.length; i++){

            randNum = (int)(Math.random()*(passArray.length));

            double temp = passArray[i];

            passArray[i] = passArray[randNum];

            passArray[randNum] = temp;

        }

    }

    public static void shift(double[] passArray){

        double loopTemp1;

        double loopTemp2 = passArray[passArray.length - 1];

        for(int i = 0; i < passArray.length; i++){

                loopTemp1 = passArray[i];

                passArray[i] = loopTemp2;

                loopTemp2 = loopTemp1;

        }

    }

}
