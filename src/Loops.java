// CMPS 280
// Program Assignment 03
// Phillip Weaver
// W0659336
// //
// This Program is brought to you by: Coffee!

import java.util.concurrent.TimeUnit;

public class Loops {
    public static void main(String args[]) throws InterruptedException {

        //provide an example of each type of loop which illustrates its advantages

        //while loop

        boolean done = false;

        int whileNum = 0;

        while (done == false){

            if(whileNum > 5) done = true;

            else{

                System.out.println("Not done (while)! " + whileNum++);

                TimeUnit.MILLISECONDS.sleep(250);

            }

        }

        System.out.print("While Loop Complete!\n\n");

        TimeUnit.MILLISECONDS.sleep(500);

        //do-while loop

        int doWhileNum = 0;

        do{

            System.out.println("Not done (do-while)! " + doWhileNum++);

            TimeUnit.MILLISECONDS.sleep(250);

        }while(doWhileNum != 0 && doWhileNum <= 5);

        System.out.print("Do-While Loop Complete!\n\n");

        TimeUnit.MILLISECONDS.sleep(500);

        //for loop

        for(int i = 0; i <= 5; i++){

            System.out.println("Not done (for)! " + i);

            TimeUnit.MILLISECONDS.sleep(250);

        }

        System.out.print("For Loop Complete!\n");

    }
}
