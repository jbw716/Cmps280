// CMPS 280
// Circle Factory
// Phillip Weaver
// W0659336
// 2/20/2017
// This Program is brought to you by: Coffee!

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Circle {

    private double radius;

    public static void main(String args[]) throws InterruptedException {

        Circle circle = new Circle();

        circle.getRadius();

        circle.printCircumference();

        circle.printArea();

        circle.printDiameter();

    }

    public void getRadius() throws InterruptedException {

        Scanner input = new Scanner(System.in);

        while(true) {

            try {

                boolean positiveInput = false;

                while(positiveInput == false) {

                    System.out.print("What is the radius of your circle (must be a positive number)?\n>");

                    positiveInput = setRadius(input.nextDouble());

                }

                break;

            } catch (InputMismatchException e) {

                System.out.println("Sorry. That is not a valid input. Please try again...");

            }

        }

    }

    public boolean setRadius(double newRadius) throws InterruptedException {

        if(newRadius < 0){

            System.out.println("Invalid radius. Input must be non-negative!");

            System.out.println("Radius unchanged.\n");

            TimeUnit.MILLISECONDS.sleep(250);

            return false;

        }

        else{

            radius = newRadius;

            printRadius();

            return true;

        }

    }

    public void printRadius(){

        System.out.println("Radius set to: " + radius);

    }

    public double getArea(){

        double area = Math.PI * (radius * radius);

        return (Math.round(area*100))/100.0;

    }

    public void printArea(){

        System.out.println("Area is: " + getArea());

    }

    public double getDiameter(){

        return radius*2;

    }

    public void printDiameter(){

        System.out.println("Diameter is: " + getDiameter());

    }

    public double getCircumference(){

        return (Math.round((2*Math.PI*radius)*100))/100.0;

    }

    public void printCircumference(){

        System.out.println("Circumference is: " + getCircumference());

    }

}
