// CMPS 280
// Regular Polygon Assignment
// Phillip Weaver
// W0659336
// 3/3/2017
// This Program is brought to you by: Coffee!

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class TestRP {

    public static void main(String[] args) throws InterruptedException {

        System.out.print("*Polygon Tool*\n\n");

        while(true) {

            Scanner input = new Scanner(System.in);

            while (true) {

                System.out.print("Would you like to change the default number of sides and length of those sides? Default is 3 sides of length 1. (y/n)\n>");

                String response = input.nextLine();

                if (response.toLowerCase().equals("y")) {

                    while (true) {

                        System.out.print("\nHow many sides would you like the polygon to have? Enter a whole number greater than or equal to 3: ");

                        int newN = input.nextInt();

                        if (newN >= 3) {

                            System.out.print("\nWhat would you like the length of the sides to be?\n>");

                            double newSide = input.nextDouble();

                            while (true) {

                                System.out.print("\nWould you like to enter custom (x,y) coordinates for the polygon? (y/n)\n>");

                                input.nextLine();

                                String response2 = input.nextLine();

                                if (response2.toLowerCase().equals("y")) {

                                    System.out.print("\nEnter the x coordinate for the center of the polygon: ");

                                    double newX = input.nextDouble();

                                    System.out.print("\nEnter the y coordinate for the center of the polygon: ");

                                    double newY = input.nextDouble();

                                    RegularPolygon rp = new RegularPolygon(newN, newSide, newX, newY);

                                    break;

                                } else if (response2.toLowerCase().equals("n")) {

                                    RegularPolygon rp = new RegularPolygon(newN, newSide);

                                    break;

                                } else {

                                    System.out.println("Sorry. That is not a valid input.");

                                }

                            }

                            break;

                        } else {

                            System.out.println("Sorry. That is not a valid input.");

                        }

                    }

                    break;

                } else if (response.toLowerCase().equals("n")) {

                    RegularPolygon rp = new RegularPolygon();

                    break;

                } else {

                    System.out.println("Sorry. That is not a valid response.");

                    TimeUnit.MILLISECONDS.sleep(250);

                }

            }

            while(true) {

                Scanner inputResponse = new Scanner(System.in);

                System.out.print("\nWould you like to run the program again? (y/n)\n>");

                String finalResponse = inputResponse.nextLine();

                if (finalResponse.toLowerCase().equals("n")) System.exit(0);

                else if (finalResponse.toLowerCase().equals("y")) break;

                else {

                    System.out.println("Sorry. That is not a valid response.");

                }

            }

        }

    }

}
