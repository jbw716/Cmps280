// CMPS 280
// Regular Polygon Assignment
// Phillip Weaver
// W0659336
// 3/3/2017
// This Program is brought to you by: Coffee!

import java.util.InputMismatchException;
import java.util.Scanner;

public class NewTestRP {

    public static void main(String[] args) {

        System.out.println("*Polygon Tool*");

        RegularPolygon[] regularPolygonArray = new RegularPolygon[3];

        regularPolygonArray[0] = new RegularPolygon(4,7);

        printRegularPolygon(regularPolygonArray[0]);

        while (true) {

            Scanner input = new Scanner(System.in);

            System.out.print("\nWould you like to change the default number of sides and length of those sides? Default is 3 sides of length 1. (y/n)\n>");

            String yn = input.nextLine();

            if (yn.toLowerCase().equals("n")) {

                RegularPolygon rp = new RegularPolygon();

                printRegularPolygon(rp);

                again();

            } else if (yn.toLowerCase().equals("y")) {

                customNS();

                again();

            }

            else{

                System.out.println("Sorry. That is not a valid input.");

            }

        }

    }

    public static void customNS(){

        Scanner input = new Scanner(System.in);

        int newN = 3;

        double newSide = 1;

        while(true) {

            System.out.print("\nHow many sides would you like the polygon to have? Enter a whole number greater than or equal to 3.\n>");

            try {

                newN = input.nextInt();

                if (newN < 3) {

                    System.out.println("Sorry. That is not a valid input.");

                    input.nextLine();

                } else break;

            } catch (InputMismatchException e) {

                System.out.println("Sorry. That is not a valid input.");

                input.nextLine();

            }

        }

        while(true) {

            System.out.print("\nWhat would you like the length of the sides to be? (Enter a number greater than 0)\n>");

            try {

                newSide = input.nextDouble();

                if (newSide <= 0) {

                    System.out.println("Sorry. That is not a valid input.");

                } else break;

            }

            catch(InputMismatchException e){

                System.out.println("Sorry. That is not a valid input.");

                input.nextLine();

            }

        }

        input.nextLine();

        while(true){

            System.out.print("\nWould you like to enter custom (x,y) coordinates for the polygon? (y/n)\n>");

            String yn = input.nextLine();

            if(yn.toLowerCase().equals("n")){

                RegularPolygon rp = new RegularPolygon(newN, newSide);

                printRegularPolygon(rp);

                break;

            }

            else if(yn.toLowerCase().equals("y")){

                customXY(newN, newSide);

                break;

            }

            else{

                System.out.println("Sorry. That is not a valid input.");

            }

        }

    }

    public static void customXY(int newN, double newSide){

        Scanner input = new Scanner(System.in);

        double newX;

        double newY;

        while(true) {

            System.out.print("\nEnter the x coordinate for the center of the polygon.\n>");

            try {

                newX = input.nextDouble();

                break;

            }

            catch(InputMismatchException e){

                System.out.println("Sorry. That is not a valid input.");

                input.nextLine();

            }

        }

        while(true) {

            System.out.print("\nEnter the y coordinate for the center of the polygon.\n>");

            try {

                newY = input.nextDouble();

                break;

            }

            catch(InputMismatchException e){

                System.out.println("Sorry. That is not a valid input.");

                input.nextLine();

            }

        }

        RegularPolygon rp = new RegularPolygon(newN, newSide, newX, newY);

        printRegularPolygon(rp);

    }

    public static void again(){

        while(true) {

            Scanner input = new Scanner(System.in);

            System.out.print("\nWould you like to run the program again? (y/n)\n>");

            String yn = input.nextLine();

            if (yn.toLowerCase().equals("n")) System.exit(0);

            else if (yn.toLowerCase().equals("y")) break;

            else System.out.println("Sorry. That is not a valid response.");

        }

    }

    public static void printRegularPolygon(RegularPolygon regPoly){

        System.out.println();

        System.out.println("Number of polygons: " + regPoly.getCount());

        System.out.println("Number of sides: " + regPoly.getN());

        System.out.println("Length of sides: " + regPoly.getSide());

        System.out.println("X value: " + regPoly.getX());

        System.out.println("Y value: " + regPoly.getY());

        //Print Perimeter

        //Print Area

    }

}
