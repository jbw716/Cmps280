// CMPS 280
// BMI Calculator Program
// Phillip Weaver
// W0659336
// 3/15/2017
// This Program is brought to you by: Coffee!

public class TestBMI {

    public static void main(String[] args){

        //Format: BMI name = new BMI("Name", gender 'm/f/o', age, wight in pounds, height in inches);

        BMI jeremiah = new BMI("Jeremiah", 17, 230, 72);

        print(jeremiah);

        BMI bob = new BMI(160, 65);

        BMI haleigh = new BMI("Haleigh", 130, 65);

        BMI becky = new BMI("Becky", 23, 120, 60);

        BMI will = new BMI('o');

        BMI jack = new BMI("Jack", 'm');

        BMI jill = new BMI("Jill", 'f', 140, 55);

        BMI suzy = new BMI("Suzy", 'o', 26, 130, 60);

        print(bob);

        print(haleigh);

        print(becky);

        print(will);

        print(jack);

        print(jill);

        print(suzy);

    }

    public static void print(BMI passed){

        System.out.println("The BMI " + ((passed.getName() != (null))? "for " + passed.getName() + " is: " : "is: ") + passed.getBMI() + " " + passed.getStatus() + "\n");

    }

}
