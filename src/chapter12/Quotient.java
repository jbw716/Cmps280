package chapter12;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Quotient {
  public static void main(String[] args) throws InterruptedException {
    Scanner input = new Scanner(System.in);
    
    // Prompt the user to enter two integers
    System.out.print("Enter two integers: ");
    int number1 = input.nextInt();
    int number2 = input.nextInt();

    /*if(number2 != 0)
      System.out.println(number1 + " / " + number2 + " is " +
        (number1 / number2));

    else{

      System.out.println("Error: System self-destruct sequence initiated");

      for(int i = 5; i > 0; i--){

        System.out.println(i);

        TimeUnit.MILLISECONDS.sleep(500);

      }

      System.out.println("BOOM!!!");

    }*/

    try {

      System.out.println(quotient(number1, number2));

    }

    catch(ArithmeticException e){

      System.out.println(e + "\nNope!");

      System.exit(1);

    }

    System.out.println("Is program still running?");

  }

  public static int quotient (int number1, int number2){

    /*if(number2 == 0){

      System.out.println("Dividing by zero - no no no");

      System.exit(1);

    }*/

    return number1/number2;

  }

}
