// CMPS 280
// Course Assignment
// Phillip Weaver
// W0659336
// 3/21/2017
// This Program is brought to you by: Coffee!

//***get and set for the scalar fields (not array)
//***get and set courseName
//***get set courseNumber
//***get set teacherName
//***get set students? NO! We need to have addStudent, and dropStudent
//***get numStudents. Not set because only changes with addStudents or dropStudents
//***TEST Course in a separate class TestCourse
//***Provide UML diagram in comments

//dropStudent CHECK BOOK

/*
UML Diagram
--------------------------------
- courseNumber: int
- courseName: String
- teacherName: String
- studentName: String
- students: String[]
- numStudents: int
--------------------------------
+ setTeacher(teacherName: String): void
+ getTeacher(): String
+ printTeacher(): void
+ setCourseName(courseName: String): void
+ getCourseName(): String
+ printCourseName(): void
+ setCourseNumber(courseNumber: int): void
+ getCourseNumber(): int
+ printCourseNumber(): void
+ addStudent(studentName: String): void
+ dropStudent(studentName: String): void
+ getStudents(): String[]
+ printStudents(): void
+ getStudentAtIndex(index: int): String
+ printStudentAtIndex(index: int): void
+ getNumStudents(): int
+ printNumStudents(): void
+ Course()
+ Course(courseName: String)
+ Course(courseName: String, courseNumber: int)
 */

public class Course {

    private int courseNumber;

    private String courseName;

    private String teacherName;

    private String studentName;

    private String[] students = new String[35];

    private int numStudents;

    public Course(){

    }

    public Course(String courseName){

        //this.courseName = courseName;

        setCourseName(courseName);

    }

    public Course(String courseName, int courseNumber){

        this(courseName);

        //this.courseNumber = courseNumber;

        setCourseNumber(courseNumber);

    }

    public void setTeacher(String teacherName){

        this.teacherName = teacherName;

    }

    public String getTeacher(){

        return teacherName;

    }

    public void printTeacher(){

        System.out.println("Teacher name: " + getTeacher());

    }

    public void setCourseName(String courseName){

        this.courseName = courseName;

    }

    public String getCourseName(){

        return courseName;

    }

    public void printCourseName(){

        System.out.println("Course name: " + getCourseName());

    }

    public void setCourseNumber(int courseNumber){

        this.courseNumber = courseNumber;

    }

    public int getCourseNumber(){

        return courseNumber;

    }

    public void printCourseNumber(){

        System.out.println("Course number: " + getCourseNumber());

    }

    public void addStudent(String studentName){

        students[numStudents] = studentName;

        numStudents++;

        System.out.println("Successfully added " + studentName + " to the course.");

    }

    public void dropStudent(String studentName){

        boolean found = false;

        int foundIndex = 0;

        for(int i = 0; i < students.length; i++){

            if(students[i] == studentName) {

                found = true;

                foundIndex = i;

                break;

            }

        }

        if(found == true) {

            //students[foundIndex] = null;

            numStudents--;

            for (int i = foundIndex; i < students.length; i++) {

                if (i == students.length - 1) {

                    students[i] = null;

                } else {

                    students[i] = students[i + 1];

                }

            }

            System.out.println("Successfully dropped " + studentName + " from the course.");

        }

        if(found == false){

            System.out.println("Student to be dropped not found!");

        }

    }

    public String[] getStudents(){

        return students;

    }

    public void printStudents(){

        System.out.println("\nPrinting course roster...");

        System.out.println("--------------------------------------------------");

        for(int i = 0; i < numStudents; i++){

            System.out.println("Student name: " + students[i]);

        }

        System.out.println("--------------------------------------------------\n");

    }

    public String getStudentAtIndex(int index){

        return students[index];

    }

    public void printStudentAtIndex(int index){

        System.out.println("Student at index " + index + " is: " + getStudentAtIndex(index));

    }

    public int getNumStudents(){

        return numStudents;

    }

    public void printNumStudents(){

        System.out.println("numStudents: " + getNumStudents());

    }

}
