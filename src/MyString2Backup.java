// CMPS 280
// MyString2Backup
// Phillip Weaver
// W0659336
// 3/29/2017
// This Program is brought to you by: Coffee!

public class MyString2Backup {

    private char[] chars;

    public static void main(String[] args){

        MyString2Backup string1 = new MyString2Backup(new char[]{'h', 'o', 'w', 'd', 'y'});

        MyString2Backup string2 = new MyString2Backup(new char[]{'h', 'o', 'w', 'd', 'y'});

        string1.printMyString2();

        System.out.println();

        string2.printMyString2();

        System.out.println();

        System.out.println(string2.compare(string1));

    }

    public MyString2Backup(char[] chars){

        this.chars = new char[chars.length];

        System.arraycopy(chars, 0, this.chars, 0, chars.length);

    }

    public void printMyString2(){

        for(int i = 0; i < chars.length; i++){

            System.out.print(chars[i]);

        }

    }

    public int compare(MyString2Backup s){

        if(s.chars.length != chars.length)

            return 0;

        for(int i = 0; i < chars.length; i++){

            if(chars[i] != s.chars[i])

                return 0;

        }

        return 1;

    }

}
