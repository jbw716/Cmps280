// CMPS 280
// BMI Calculator
// Phillip Weaver
// W0659336
// 3/15/2017
// This Program is brought to you by: Coffee!

// pg. 371

/*

UML Diagram
---------------------------
- name: String | Default: none
- age: int | Default: none
- gender: char | Default: none
- height: double | Default: If male, 180. If female, 147. If other, 163.5.
- weight: double | Default: If male, 70. If female, 65. If other, 67.5.
---------------------------------------------------------------------------
+ BMI(weight: double, height: double)
+ BMI(name: String, weight: double, height: double)
+ BMI(name: String, age: int, weight: double, height: double)
+ BMI(gender: char)
+ BMI(name: String, gender: char)
+ BMI(name: String, gender: char, weight: double, height: double)
+ BMI(name: String, gender: char, age: int, weight: double, height: double)
+ getWeight(): double
+ getHeight(): double
+ getAge(): int
+ getName(): String
+ getGender(): char
+ getBMI(): double

 */

public class BMI {

    private String name;

    private int age;

    private char gender;

    private double height;

    private double weight;

    // If weight and height are not given, require gender for estimation.

    public BMI(double nWeight, double nHeight){

        weight = (nWeight * 0.45);

        height = (nHeight * 0.0254);

    }

    public BMI(String nName, double nWeight, double nHeight){

        this(nWeight, nHeight);

        name = nName;

    }

    public BMI(String nName, int nAge, double nWeight, double nHeight){

        this(nName, nWeight, nHeight);

        age = nAge;

    }

    public BMI(char nGender){

        gender = nGender;

        if(gender == 'm') {

            weight = (180 * 0.45);

            height = (70 * 0.0254);

        }

        else if(gender == 'f') {

            weight = (147 * 0.45);

            height = (65 * 0.0254);

        }

        else if(gender == 'o') {

            weight = (163.5 * 0.45);

            height = (67.5 * 0.0254);

        }

    }

    public BMI(String nName, char nGender){

        //gender = nGender;

        this(nGender);

        name = nName;

    }

    public BMI(String nName, char nGender, double nWeight, double nHeight){

        //name = nName;

        //gender = nGender;

        this(nName, nGender);

        weight = (nWeight * 0.45);

        height = (nHeight * 0.0254);

    }

    public BMI(String nName, char nGender, int nAge, double nWeight, double nHeight){

        //name = nName;

        //gender = nGender;

        //weight = (nWeight * 0.45);

        //height = (nHeight * 0.0254);

        this(nName, nGender, nWeight, nHeight);

        age = nAge;

    }

    public double getWeight(){

        return weight;

    }

    public double getHeight(){

        return height;

    }

    public int getAge(){

        return age;

    }

    public String getName(){

        return name;

    }

    public char getGender(){

        return gender;

    }

    public double getBMI(){

        return (Math.round(1000*(weight/(height * height))))/1000.0;

    }

    public String getStatus(){

        double bmi = getBMI();

        if(bmi < 18.5)

            return "Underweight";

        if(bmi < 25)

            return "Normal";

        if(bmi < 30)

            return "Overweight";

        else

            return "Obese";

    }

}
