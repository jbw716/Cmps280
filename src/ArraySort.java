// CMPS 280
// Program Assignment 04
// Phillip Weaver
// W0659336
// 2/11/2017
// This Program is brought to you by: Coffee!

import java.util.*;
import java.util.concurrent.TimeUnit;

public class ArraySort {

    public static void main(String[] args) throws InterruptedException {

        while(true) {

            Scanner input = new Scanner(System.in);

            List<Double> list = new ArrayList<>();

            int count = 0;

            while (true) {

                System.out.print("\nPlease enter a number to add to the list of numbers and then press 'ENTER'.\nIf you are finished entering numbers, press 'ENTER' without entering anything into the console.\n>");

                String inputString = input.nextLine();

                if(list.size() == 0 && inputString.equals("")){

                    System.out.println("\nEnding program...");

                    return;

                }

                if (inputString.equals("")) break;

                else {

                    Scanner stringScanner = new Scanner(inputString);

                    while(true) {

                        try {

                            list.add(stringScanner.nextDouble());

                            count++;

                        }
                        catch(NoSuchElementException e){

                            try{

                                list.get(0);

                                System.out.println("\nThe last successfully processed number was " + list.get(count-1) + ". If this is incorrect, simply reenter all of the numbers following " + list.get(count-1) + ".");

                                //TimeUnit.MILLISECONDS.sleep(300);

                                break;

                            }
                            catch(IndexOutOfBoundsException exc){

                                System.out.println("\nSorry. That is not a valid input. Please try again.");

                                TimeUnit.MILLISECONDS.sleep(300);

                                break;

                            }

                        }

                    }

                }

            }

            double[] doubleArray = new double[list.size()];

            for (int i = 0; i < list.size(); i++)
                doubleArray[i] = list.get(i);

            selectionSort(doubleArray);

            while(true) {

                if (binarySearch(doubleArray) == false) break;

            }

            boolean respBool;

            while(true) {

                System.out.print("\nWould you like to run the program again? (y/N)\n>");

                String response = input.nextLine();

                if (response.toLowerCase().equals("y")){

                    respBool = true;

                    break;

                }

                else if (response.toLowerCase().equals("n")){

                    respBool = false;

                    break;

                }

                else if (response.equals("")){

                    respBool = false;

                    break;

                }

                else System.out.println("Sorry. That is not a valid response.\n");

            }

            if(respBool == false){

                System.out.println("\nEnding program...");

                break;

            }

        }

    }

    public static void selectionSort(double[] passArray){

        System.out.println("\nSorting list...\n");

        //Arrays.sort(passArray);

        for (int i = 0; i < passArray.length - 1; i++){

            double currentMin = passArray[i];

            int currentMinIndex = i;

            for(int j = i + 1; j < passArray.length; j++){

                if(currentMin > passArray[j]){

                    currentMin = passArray[j];

                    currentMinIndex = j;

                }

            }

            if(currentMinIndex != i){

                passArray[currentMinIndex] = passArray[i];

                passArray[i] = currentMin;

            }

        }

        for(int i = 0; i < passArray.length; i++) System.out.println("index " + i + ": " + passArray[i]);

    }

    public static boolean binarySearch(double[] passArray) throws InterruptedException {

        Scanner input = new Scanner(System.in);

        System.out.print("\nWhat number(s) would you like to search for in your given number list?\n>");

        String inputString = input.nextLine();

        Scanner stringScanner = new Scanner(inputString);

        while(true){

            double key;

            int l = 0;

            int h = passArray.length - 1;

            try {

                key = stringScanner.nextDouble();

            }
            catch(NoSuchElementException e){

                break;

            }

            while(h >= l) {

                int m = ((l + h) / 2);

                //System.out.println("While");

                //TimeUnit.MILLISECONDS.sleep(500);

                //System.out.println("M: " + m);

                //System.out.println("H: " + h);

                //System.out.println("L: " + l);

                if(key < passArray[m]){

                    h = m - 1;

                }

                else if (passArray[m] == key) {

                    System.out.print("\n" + key + " was found at index " + m + " after auto sorting!\n");

                    break;

                } /*else if (h == l) {

                    System.out.print("\n*Sorry. " + key + " was not found in the given list.*\n");

                    break;

                }*/ else if (passArray[m] < key) {

                    l = m + 1;

                }

                if(!(h >= l)){

                    System.out.print("\n*Sorry. " + key + " was not found in the given list.*\n");

                }

            }

        }

        System.out.print("\nWould you like to search for more numbers within the given list? (y/N)\n>");

        /*double key;

        int l = 0;

        int h = passArray.length - 1;

        while(true){

            System.out.print("\nWhat one number would you like to search for in your number list?\n>");

            try {

                key = input.nextDouble();

                break;

            }

            catch(InputMismatchException e){

                System.out.println("Sorry. That is not a valid input.\n");

                TimeUnit.MILLISECONDS.sleep(300);

            }

        }

        while(true) {

            int m = ((l + h) / 2);

            if(passArray[m] == key) {

                System.out.print("\n" + key + " found at index " + m + " after auto sorting!\n\nWould you like to search for another number in the given list? (y/N)\n>");

                break;

            }

            else if(h == l){

                System.out.print("\nSorry. That number was not found in the given list.\nWould you like to search for another number in the given list? (y/N)\n>");

                break;

            }

            else if(passArray[m] > key){

                h = m-1;

            }

            else if(passArray[m] < key){

                l = m+1;

            }

        }*/

        boolean respBool;

        while(true) {

            String response = input.nextLine();

            if (response.toLowerCase().equals("y")){

                respBool = true;

                break;

            }

            else if (response.toLowerCase().equals("n")){

                respBool = false;

                break;

            }

            else if (response.equals("")){

                respBool = false;

                break;

            }

            else System.out.print("\nSorry. That is not a valid response.\nWould you like to search for another number in the given list? (y/N)\n>");

        }

        if(respBool == false) return false;

        else return true;

    }

}

//Test Scenario:

//9 2 6 4 8 99 0 44 85 934.72 85 993 583

//4 8 85 934.72 2 76