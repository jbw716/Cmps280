// CMPS 280
// Program Assignment 03
// Phillip Weaver
// W0659336
// 2/2/2017
// This Program is brought to you by: Coffee!

public class ArraysPart1 {

    public static void main(String[] args){

        int[] myList = new int[10];

        System.out.print("Initial Array: ");

        printIntArray(myList);

        System.out.print("\n\n");

        System.out.print("Array Initialized With Input Values: ");

        myList = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        printIntArray(myList);

        System.out.print("\n\n");

        System.out.print("Array Initialized With Index Values: ");

        initToIndex(myList);

        printIntArray(myList);

        System.out.print("\n\n");

        //System.out.println("findSmallest(myList): " + findSmallest(myList));

        System.out.print("Array Initialized With Random Values: ");

        randInit(myList);

        printIntArray(myList);

        System.out.print("\n");

        //System.out.println("findSmallest(myList): " + findSmallest(myList));

    }

    public static void printIntArray(int[] intArray){

        System.out.print("\n{");

        for(int i = 0; i < intArray.length; i++){

            System.out.print(intArray[i]);

            if(i != (intArray.length - 1)) System.out.print(", ");

            else System.out.print("}");

        }

    }

    public static void initToIndex(int[] intArray){

        for(int i = 0; i < intArray.length; i++){

            intArray[i] = i;

        }

    }

    /*public static int findSmallest(int[] intArray){

        int small = intArray[0];

        for(int i = 0; i < intArray.length; i++){

            if(intArray[i] < small) small = intArray[i];

        }

        return small;

    }*/

    public static void randInit(int[] intArray){

        for(int i = 0; i < intArray.length; i++){

            intArray[i] = (int)(Math.random() * 100);

        }

    }

}
