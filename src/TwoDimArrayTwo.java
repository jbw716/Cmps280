// CMPS 280
// Two Dimensional Array Assignment 2
// Phillip Weaver
// W0659336
// 2/18/2017
// This Program is brought to you by: Coffee!

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class TwoDimArrayTwo {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        double[][] doubleArray;

        while(true) {

            System.out.print("\nWould you like to create an two-dimensional array with random values? (y/N)\n>");

            String response = input.nextLine();

            if (response.toLowerCase().equals("y")) {

                System.out.print("\nHow many sets of numbers would you like to have?\n>");

                int i1 = input.nextInt();

                System.out.print("\nHow many numbers would you like to have per set?\n>");

                int i2 = input.nextInt();

                doubleArray = randomArray(i1, i2);

                break;

            } else if(response.toLowerCase().equals("n") || response.toLowerCase().equals("")){

                System.out.print("\nHow many sets of numbers would you like to have?\n>");

                int l1 = input.nextInt();

                System.out.print("\nHow many numbers would you like to have per set?\n>");

                int l2 = input.nextInt();

                doubleArray = new double[l1][l2];

                for (int i = 0; i < doubleArray.length; i++) {

                    for (int j = 0; j < doubleArray[i].length; j++) {

                        System.out.print("\nEnter number " + (j + 1) + " for set " + (i + 1) + ".\n>");

                        double inputDouble = input.nextDouble();

                        doubleArray[i][j] = inputDouble;

                    }

                }

                break;

            } else{

                System.out.print("\nSorry. That is not a valid response...\n");

            }

        }

        /*printDoubleTwoDim(doubleArray);

        System.out.print("\nThe sum of all of the entered numbers is: " + sumDouble2DArray(doubleArray));

        printDoubleOneDim(sumByColumn(doubleArray));

        System.out.print("\n\nThe row with the largest number in it is row " + rowWithLargest(doubleArray) + ".");

        System.out.println(smIndexLGElement(doubleArray));

        printDoubleTwoDim(shuffle2DA(doubleArray));*/

        exeAllMethods(doubleArray);

        System.out.print("\n----------------------------------------------------------------------------------------------------\n");

        exeAllMethods(doubleArray);

    }

    public static void exeAllMethods(double[][] doubleArray){

        printDoubleTwoDim(doubleArray);

        System.out.print("\nThe sum of all of the entered numbers is: " + sumDouble2DArray(doubleArray));

        printDoubleOneDim(sumByColumn(doubleArray));

        System.out.print("\n\nThe row with the largest number in it is row " + rowWithLargest(doubleArray) + ".");

        System.out.println(smIndexLGElement(doubleArray));

        printDoubleTwoDim(shuffle2DA(doubleArray));

    }

    public static void printDoubleOneDim(double[] passArray){

        System.out.println();

        for(int i = 0; i < passArray.length; i++){

            System.out.print("\nThe sum of column " + i + " is: " + passArray[i]);

        }

    }

    public static void printDoubleTwoDim(double pass2d[][]){

        System.out.print("\n\n*Printing 2D array...\n");

        System.out.println();

        for(int i = 0; i < pass2d[(i > (pass2d.length - 1)) ? (pass2d.length - 1) : i].length; i++) {

            for (int j = 0; j < pass2d.length; j++) {

                System.out.print("[" + j + "][" + i + "] is: " + pass2d[j][i] + "\t");

            }

            System.out.println();

        }

    }

    public static double sumDouble2DArray(double[][] pass2DA){

        System.out.print("\n\n*Summing 2D array...\n");

        double sum = 0;

        for(int j = 0; j < pass2DA.length; j++){

            for(int i = 0; i < pass2DA[j].length; i++){

                sum += pass2DA[j][i];

            }

        }

        return sum;

    }

    public static double[] sumByColumn(double[][] pass2DA){

        System.out.print("\n\n\n*Summing 2D array by column...");

        double[] returnArray = new double[pass2DA.length];

        for(int i = 0; i < pass2DA.length; i++){

            double sum = 0;

            for(int j = 0; j < pass2DA[i].length; j++){

                sum += pass2DA[i][j];

            }

            returnArray[i] = sum;

        }

        return returnArray;

    }

    public static int rowWithLargest(double[][] passArray){

        System.out.print("\n\n\n*Finding the row with the largest element...");

        double[] maxes = new double[passArray[1].length];

        int returnRow = 0;

        for(int i = 0; i < passArray[(i > (passArray.length - 1)) ? (passArray.length - 1) : i].length; i++){

            double max = passArray[0][i];

            for(int j = 0; j < passArray.length; j++){

                if(passArray[j][i] > max) max = passArray[j][i];

            }

            maxes[i] = max;

        }

        for(int i = 0; i < maxes.length; i++){

            if(maxes[i] > maxes[returnRow]) returnRow = i;

        }

        System.out.println();

        for(int i = 0; i < maxes.length; i++){

            System.out.print("\nThe max number in row " + i + " is: " + maxes[i]);

        }

        return returnRow;

    }

    public static String smIndexLGElement(double[][] pass2d){

        System.out.print("\n\n\n*Finding the smallest index of the largest element...");

        int i1 = 0;

        int i2 = 0;

        for(int i = 0; i < pass2d.length; i++) {

            for (int j = 0; j < pass2d[i].length; j++) {

                if(pass2d[i1][i2] < pass2d[i][j]){

                    i1 = i;

                    i2 = j;

                }

            }

        }

        return ("\n\nThe largest element in the array is " + pass2d[i1][i2] + " and the smallest index of it in the array is [" + i1 + "][" + i2 + "].");

    }

    public static double[][] shuffle2DA(double[][] passArray){

        System.out.print("\n\n*Shuffling 2-dimensional array...");

        //double[] storage = new double[passArray[1].length * passArray.length];

        //double[][] returnArray = new double[passArray.length][passArray[1].length];

        List<Double> storageList = new ArrayList<>();

        for(int i = 0; i < passArray[(i > (passArray.length - 1)) ? (passArray.length - 1) : i].length; i++){

            for(int j = 0; j < passArray.length; j++){

                storageList.add(passArray[j][i]);

            }

        }

        //System.out.print("\nShuffling 2-dimensional array...\n");

        Collections.shuffle(storageList);

        for(int i = 0; i < passArray.length; i++){

            for(int j = 0; j < passArray[i].length; j++){

                //returnArray[i][j] = storageList.get((i > 0) ? ((passArray[1].length * i) + j) : j);

                passArray[i][j] = storageList.get((i > 0) ? ((passArray[1].length * i) + j) : j);

            }

        }

        /*for(int i = 0; i < returnArray[(i > (returnArray.length - 1)) ? (returnArray.length - 1) : i].length; i++) {

            for (int j = 0; j < returnArray.length; j++) {

                System.out.print("[" + j + "][" + i + "] is: " + returnArray[j][i] + "\t");

            }

            System.out.println();

        }*/

        //return returnArray;

        return passArray;

    }

    public static double[][] randomArray(int i1, int i2){

        System.out.print("\n\n*Generating random values for the array...\n");

        double[][] returnArray = new double[i1][i2];

        for(int i = 0; i < returnArray[(i > (returnArray.length - 1)) ? (returnArray.length - 1) : i].length; i++) {

            for (int j = 0; j < returnArray.length; j++) {

                returnArray[j][i] = Math.round(Math.random()*10000)/100.0;

            }

        }

        return returnArray;

    }

}
