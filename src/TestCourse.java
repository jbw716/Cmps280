// CMPS 280
// Course Assignment (Test)
// Phillip Weaver
// W0659336
// 3/21/2017
// This Program is brought to you by: Coffee!

public class TestCourse {

    public static void main(String[] args){

        Course cmps280 = new Course("CMPS", 280);

        cmps280.setTeacher("Steel Russel");

        cmps280.printCourseName();

        cmps280.printCourseNumber();

        cmps280.printTeacher();

        cmps280.addStudent("Phillip Weaver");

        cmps280.addStudent("Grant Falcon");

        cmps280.printNumStudents();

        cmps280.printStudents();

        cmps280.printStudentAtIndex(0);

        cmps280.dropStudent("Phillip Weaver");

        cmps280.printStudents();

        cmps280.printStudentAtIndex(0);

    }

}
