package GeometricObjectSquareCasting;
// CMPS 280
// Stack
// Phillip Weaver
// W0659336
// 4/18/2017
// This Program is brought to you by: Coffee!

import java.util.Scanner;

public class Exercise_11_10 {
	/** Main method */
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		Stack stack = new Stack();

		System.out.print("Enter five strings: \n");

		for (int i = 0; i < 5; i++) {

		    System.out.print("String " + (i + 1) + ": ");

			stack.push(input.nextLine());

		}

		System.out.println("Stack: " + stack.toString());
	}
}