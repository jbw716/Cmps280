package GeometricObjectSquareCasting;
// CMPS 280
// Stack
// Phillip Weaver
// W0659336
// 4/18/2017
// This Program is brought to you by: Coffee!

/*****************************
 *           MyStack          *
 *----------------------------*
 * -list: ArrayList<Object>   *
 *                            *
 * +Stack()                   *
 *                            *
 * +push(ojb: Object): void   *
 *                            *
 * +peek(): Object            *
 *                            *
 * +pop(): Object             *
 *                            *
 * +toString(): String        *
 *****************************/

public class Stack extends java.util.ArrayList {

    private java.util.ArrayList<Object> list;

    public Stack(){

        list = new java.util.ArrayList<Object>();

    }

    /** Add a new element to
     * the top of this stack */
    public void push(Object obj) {

        list.add(0, obj);

    }

    public Object peek() {

        return list.get(0);

    }

    public Object pop() {

        Object obj = list.get(0);

        list.remove(0);

        return obj;
    }

    @Override
    public String toString() {

        return list.toString();

    }

}
