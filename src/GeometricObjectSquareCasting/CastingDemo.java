package GeometricObjectSquareCasting;
// CMPS 280
// Square Casting
// Phillip Weaver
// W0659336
// 4/11/2017

public class CastingDemo {
  /** Main method */
  public static void main(String[] args) {

    GeometricObject geoObjArray[] = new GeometricObject[5];

    // Declare and initialize two objects
    GeometricObject geoObject1 = new Circle(1);
    GeometricObject geoObject2 = new Rectangle(1, 2);
    GeometricObject geoObject3 = new Square(1);

    /*// Display circle and rectangle
    //System.out.println("\nCircle:");
    displayGeometricObject(geoObject1);
    //System.out.println("\nRectangle:");
    displayGeometricObject(geoObject2);
    //System.out.println("\nSquare:");
    displayGeometricObject(geoObject3);*/

    geoObjArray[0] = geoObject1;
    geoObjArray[1] = geoObject2;
    geoObjArray[2] = geoObject3;
    geoObjArray[3] = new Circle(2);
    geoObjArray[4] = new Rectangle(5, 5);

    displayGeometricObjectArray(geoObjArray);

  }

  /** A method for displaying an object */
  /*public static void displayGeometricObject(GeometricObject object) {
    if (object instanceof Circle) {
      System.out.println("\tThe circle area is " +
              ((Circle)object).getArea());
      System.out.println("\tThe circle diameter is " +
              ((Circle)object).getDiameter());
    }
    else if (object instanceof Square) {
      System.out.println("\tThe square area is " +
              ((Square)object).getArea());
      System.out.println("\tThe square perimeter is " +
              ((Square)object).getPerimeter());
      System.out.println("\tThe square is " +
              ((Square)object).getSideLen() + " x " + ((Square)object).getSideLen());
    }
    else if (object instanceof Rectangle) {
      System.out.println("\tThe rectangle area is " +
              ((Rectangle)object).getArea());
      System.out.println("\tThe rectangle perimeter is " +
              ((Rectangle)object).getPerimeter());
      System.out.println("\tThe rectangle is " +
              ((Rectangle)object).getWidth() + " x " + ((Rectangle)object).getHeight());
    }
  }*/

  public static void displayGeometricObject(GeometricObject object) {
    if (object instanceof Circle) {
      System.out.println("\nCircle:");
      System.out.println("\tThe circle area is " +
              ((Circle)object).getArea());
      System.out.println("\tThe circle diameter is " +
              ((Circle)object).getDiameter());
    }
    else if (object instanceof Rectangle) {
      System.out.println((object instanceof Square) ? "\nSquare:" : "\nRectangle:");
      System.out.println("\tThe " + ((object instanceof Square)?"square":"rectangle") + " area is " +
              ((Rectangle)object).getArea());
      System.out.println("\tThe " + ((object instanceof Square)?"square":"rectangle") + " perimeter is " +
              ((Rectangle)object).getPerimeter());
      System.out.println("\tThe " + ((object instanceof Square) ? ("square is " + ((Square)object).getSideLen() + " x " + ((Square)object).getSideLen()) : "rectangle is " +
              ((Rectangle)object).getWidth() + " x " + ((Rectangle)object).getHeight()));
    }
  }

  public static void displayGeometricObjectArray(GeometricObject[] passedArray){

    for(int i = 0; i < passedArray.length; i++){

      displayGeometricObject(passedArray[i]);

    }

  }

}