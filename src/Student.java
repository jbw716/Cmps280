// CMPS 280
// Student
// Phillip Weaver
// W0659336
// 3/8/2017
// This Program is brought to you by: Coffee!

import java.util.Date;

public class Student {

    private String name;

    private String wNumber;

    private double gpa;

    private Date regDate;

    public static void main(String[] args){

        Student student = new Student();

        System.out.println("gpa " + student.gpa + " hour " + student.regDate.getHours());

        Student denver = new Student("Denver", "w0123456");

        System.out.println("\nname " + denver.getName() + " wNumber " + denver.getWNumber() + "date " + denver.regDate.getHours());

        Student[] studentArray = new Student[1];

        studentArray[0] = new Student("A", "w1");

        System.out.println("\nhours for student[0] " + studentArray[0].regDate.getHours());

    }

    Student(){

        regDate = new Date();

    }

   Student(String name, String wNumber){

        this();

        this.name = name;

        this.wNumber = wNumber;

    }

    public String getName(){

        return name;

    }

    public String getWNumber(){

        return wNumber;

    }

   /*
    Student(String name, String wNumber, double gpa, Date regDate){




    }*/

}

/*

UML Diagram

- name: String

- wNumber: String

- gpa: double

- regDate: Date

 */
