package NewStack;
// CMPS 280
// NewStack
// Phillip Weaver
// W0659336
// 4/18/2017
// This Program is brought to you by: Coffee!

/******************************
 *          NewStack          *
 *----------------------------*
 * -list: ArrayList<Object>   *
 *                            *
 * +Stack()                   *
 *                            *
 * +push(ojb: Object): void   *
 *                            *
 * +peek(): Object            *
 *                            *
 * +pop(): Object             *
 *                            *
 * +toString(): String        *
 *****************************/

public class NewStack extends java.util.ArrayList {

    private java.util.ArrayList<Object> list;

    public NewStack(){

        list = new java.util.ArrayList<Object>();

    }

    /** Add a new element to
     * the top of this stack */
    public void push(Object obj) {

        list.add(0, obj);

    }

    public Object peek() {

        return list.get(0);

    }

    public Object pop() {

        Object obj = list.get(0);

        list.remove(0);

        return obj;
    }

    @Override
    public String toString() {

        return list.toString();

    }

}
