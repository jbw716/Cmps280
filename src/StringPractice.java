// CMPS 280
// Program Assignment 01
// Phillip Weaver
// W0659336
// 03/22/2017
// This Program is brought to you by: Coffee!

public class StringPractice {

    public static void main(String[] args) {

        String s1 = "one";

        String s2 = new String("one");

        String s3 = "one";

        System.out.println("s1 == s2 " + (s1 ==s2));

        System.out.println("s1 == s3 " + (s1 == s3));

    }

}
