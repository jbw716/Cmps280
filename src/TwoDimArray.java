// CMPS 280
// Program Assignment 06
// Phillip Weaver
// W0659336
// 2/14/2017
// This Program is brought to you by: Coffee!

import java.util.Scanner;

public class TwoDimArray {

    public static void main(String[] args){

        /*double[][] classGrades = {{99, 98, 97, 96, 95},
                               {89, 88, 87, 86, 85},
                               {79, 78, 77, 76, 75},
                               {69, 68, 67, 66, 65}};*/

        Scanner input = new Scanner(System.in);

        System.out.print("\nHow many sets of grades would you like to have?\n>");

        int l1 = input.nextInt();

        System.out.print("\nHow many grades would you like per set?\n>");

        int l2 = input.nextInt();

        double[][] classGrades = new double[l1][l2];

        for(int i = 0; i < classGrades.length; i++){

            for(int j = 0; j < classGrades[i].length; j++){

                System.out.print("\nEnter grade " + (j+1) + " for grade set " + (i+1) + ".\n>");

                double inputGrade = input.nextDouble();

                classGrades[i][j] = inputGrade;

            }

        }

        printOneRow(classGrades);

        printAvgs(studentAvg(classGrades));

        System.out.println("\nThe class average is: " + classAvg(classGrades));

    }

    public static void printAvgs(double[] passArray){

        System.out.println();

        for(int i = 0; i < passArray.length; i++){

            System.out.println("Student " + (i+1) + "'s average is: " + passArray[i]);

        }

    }

    public static void printOneRow (double[][] pass2d){

        for(int i = 0; i < pass2d[(i > (pass2d.length - 1)) ? (pass2d.length - 1) : i].length; i++) {

            for (int j = 0; j < pass2d.length; j++) {

                System.out.print("classGrades[" + j + "][" + i + "] is: " + pass2d[j][i] + "\t");

            }

            System.out.println();

        }

    }

    public static double[] studentAvg (double[][] pass2d){

        double[] returnArray = new double[pass2d.length];

        for(int i = 0; i < pass2d.length; i++) {

            double stAvg = 0;

            int j;

            for (j = 0; j < pass2d[i].length; j++) {

                stAvg += pass2d[i][j];

            }

            stAvg /= j;

            returnArray[i] = stAvg;

            //System.out.println("Student " + (i+1) + "'s average is: " + stAvg);

        }

        return returnArray;

    }

    public static double classAvg (double[][] pass2d){

        double clsAvg = 0;

        int count = 0;

        for(int i = 0; i < pass2d.length; i++) {

            for (int j = 0; j < pass2d[i].length; j++, count++) {

                clsAvg += pass2d[i][j];

            }

        }

        clsAvg /= count;

        return clsAvg;

        //System.out.println("\nThe class average is: " + clsAvg);

    }

}
