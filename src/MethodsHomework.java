// CMPS 280
// Program Assignment 03
// Phillip Weaver
// W0659336
// 1/26/2017
// This Program is brought to you by: Coffee!

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class MethodsHomework {

    public static void main(String[] args) throws InterruptedException {

        Scanner input = new Scanner(System.in);

        while(true) {

            //System.out.print("********************************************************\n* Please select which process you would like to run... *\n********************************************************\n* 1. Input numeric grade and receive letter grade.     *\n* 2. Print out numbers between two given numbers.      *\n* 3. Calculate the sum of all given numbers.           *\n********************************************************\n> ");

            System.out.print("********************************************************\n* Please select which process you would like to run... *\n********************************************************\n1. Input numeric grade and receive letter grade.\n2. Print out numbers between two given numbers.\n3. Calculate the sum of all given numbers.\n4. Exit\n\n>");

            String response = input.nextLine();

            if (response.equals("1")) {

                while(true) {

                    System.out.print("\nPlease enter the number or letter grade that you received and then press 'ENTER' to display the corresponding letter grade or grade range accordingly. (eg. 98 or A)\n>");

                    String userInput = input.nextLine();

                    //Error check. Throw error and restart the loop of the user entered more than one character.

                    if (userInput.length() > 1) {

                        try{

                            double userInputDouble = Double.parseDouble(userInput);

                            String result = numGrade(userInputDouble);

                            if(result.equals("\nAn error occurred!\n"))
                                System.out.print(result);

                            else{

                                System.out.print(result);

                                break;

                            }

                        } catch(NumberFormatException e) {

                            System.out.print("\nAn error occurred!\n");

                            TimeUnit.MILLISECONDS.sleep(500);

                            continue;

                        }

                    }

                    //If "end" and error checks passed, continue to the switch statment.

                    else {

                        char grade = userInput.charAt(0);

                        String result = grade(grade);

                        if(result.equals("\nAn error occurred!\n")){

                            System.out.print(result);

                            TimeUnit.MILLISECONDS.sleep(500);

                        }

                        else {

                            System.out.print(result);

                            break;

                        }

                    }

                }

                //break;

            } else if (response.equals("2")) {

                while(true){

                    System.out.print("\nPlease enter the two integers that you would like to print, along with every integer between them.\n>");

                    try{

                        int int1 = input.nextInt();

                        int int2 = input.nextInt();

                        System.out.print("\n" + numList(int1, int2) + ".\n");

                        input.nextLine();

                        break;

                    }

                    catch(InputMismatchException e){

                        System.out.print("\nSorry. That is not a valid input...\n");

                        TimeUnit.MILLISECONDS.sleep(500);

                        input.nextLine();

                    }

                }

                //break;

            } else if (response.equals("3")) {

                System.out.print("The sum of your given numbers is " + sum() + ".\n");

                //break;

            } else if(response.equals("4")){

                System.out.print("\nProgram ending...\n");

                break;

            } else {

                System.out.print("\nSorry. That is not a valid selection...\n");

                TimeUnit.MILLISECONDS.sleep(500);

            }

            TimeUnit.MILLISECONDS.sleep(500);

            System.out.print("________________________________________________________________\n\n");

        }

    }

    public static String numGrade(double userInputDouble) throws InterruptedException {

        System.out.print("\nYou entered: " + userInputDouble + "%\n");

        TimeUnit.MILLISECONDS.sleep(500);

        if (userInputDouble > 100) return("\nAn error occurred!\n");

        else if (userInputDouble >= 90) return("\nYou received an A! Good job!\n");

        else if (userInputDouble >= 80) return("\nYou received a B! Good job!\n");

        else if (userInputDouble >= 70) return("\nYou received a C.\n");

        else if (userInputDouble >= 60) return("\nYou received a D.\n");

        else if (userInputDouble >= 0) return("\nYou received an F. Uh-oh...\n");

        else if (userInputDouble < 0) return("\nAn error occurred!\n");

        else return("\nAn error occurred!\n");

    }

    public static String grade(char gradeInput) throws InterruptedException {

        String returnString;

        switch (gradeInput){

            case 'A':

            case 'a':
                returnString = ("\n90-100\n");
                break;

            case 'B':

            case 'b':
                returnString = ("\n80-89\n");
                break;

            case 'C':

            case 'c':
                returnString = ("\n70-79\n");
                break;

            case 'D':

            case 'd':
                returnString = ("\n60-69\n");
                break;

            case 'F':

            case 'f':
                returnString = ("\n0-59\n");
                break;

            //Throw an error and restart the loop if the user input one character, but it is not a valid letter grade.

            default:

                returnString = ("\nAn error occurred!\n");
                break;

        }

        return returnString;

    }

    public static String numList(int int1, int int2){

        String returnString = "";

        if(int1 > int2){

            int temp = int1;

            int1 = int2;

            int2 = temp;

        }

        int count = 0;

        for(int i = int1; i <= int2; i++){

            if(returnString.equals("")){

                returnString = returnString + i;

            }

            else if(count == 25) {

                returnString = returnString + ",\n" + i;

                count = 0;

            }

            else {

                returnString = returnString + ", " + i;

            }

            count++;

        }

        return returnString;

    }

    public static double sum() throws InterruptedException {

        Scanner input = new Scanner(System.in);

        double returnDouble = 0;

        while(true) {

            System.out.print("\nPlease enter a number or numbers and then press 'ENTER'.\nIf you are finished entering numbers, press 'ENTER' without entering any other numbers.\n>");

            String userInput = input.nextLine();

            Scanner userInputScanner = new Scanner(userInput);

            if(userInput.equals("")) return Math.round(returnDouble*1000.0)/1000.0;

            double userInputDouble = 0;

            boolean init = false;

            while(true) {

                try {

                    userInputDouble = userInputScanner.nextDouble();

                    init = true;

                    returnDouble += userInputDouble;

                } catch (NoSuchElementException e) {

                    if(e instanceof InputMismatchException) {
                        if (init == true) {

                            System.out.print("\nSorry. That is not a valid input. The last number that was processed was " + userInputDouble + " from the input, \"" + userInput + ".\"\\n");

                        }

                        else {

                            System.out.print("\nSorry. That is not a valid input. You entered, \"" + userInput + ".\"\n");

                        }

                        TimeUnit.MILLISECONDS.sleep(500);

                    }

                    break;

                }

            }

        }

    }

}